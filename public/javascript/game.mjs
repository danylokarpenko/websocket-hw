import { createElement, addClass, removeClass, getTextById, getProgressPercentage } from "./helper.mjs";
import { createRoomCard, fillRoomInfo, renderUsersOnPage } from "./components.mjs";
import { showRatingModal } from './modal/rating.mjs';

let onKeyDown;
let gameTimerId = null;
let timeoutId = null;

const stopTimer = () => {
  clearInterval(gameTimerId);
}
const stopTimeout = () => {
  clearTimeout(timeoutId);
}

const username = sessionStorage.getItem("username");
if (!username) {
  window.location.replace("/login");
}

const socket = io("", { query: { username } });

const roomsContainer = document.getElementById("rooms-container");
const roomsPage = document.getElementById("rooms-page");
const gamePage = document.getElementById("game-page");
const arena = document.getElementById("arena");
const createRoomBtn = document.getElementById("create-room-btn");

createRoomBtn.addEventListener("click", () => {
  const roomId = window.prompt('Enter room title:');
  if (!roomId) return;
  socket.emit('CREATE_ROOM', roomId);
});

const updateRooms = rooms => {
  const allRooms = rooms
    .filter(({ disable }) => !disable)
    .map(room => createRoomCard(socket, room));
  roomsContainer.innerHTML = "";
  roomsContainer.append(...allRooms);
};

const joinRoomDone = ({ connectedUsers = [] }) => {
  addClass(roomsPage, 'display-none');
  removeClass(gamePage, 'display-none');
  removeClass(gamePage, 'game-page');

  renderUsersOnPage({ connectedUsers, username });
}

const onUpdateRoom = ({ connectedUsers = [] }) => {
  if (connectedUsers.length === 0) {
    return;
  }
  renderUsersOnPage({ connectedUsers, username });
}

socket.on('USER_ACTIVE', (username) => {
  sessionStorage.removeItem("username");
  window.location.replace("/login");
  window.alert(`User with username '${username}' is active!`)
})

const onShowModal = ({ connectedUsers, roomId }) => {
  showRatingModal(connectedUsers, roomId);
}

const indicatorDone = ({ userId, progressPercentage, roomId }) => {
  const progressIndicator = document.getElementById(`${userId}-user-indicator`);
  progressIndicator.style.width = `${progressPercentage}%`;
  if (progressPercentage === 100) {
    addClass(progressIndicator, 'full-indicator');
    socket.emit('user_finished', { userId, roomId });
  }
};

const onGameEnd = ({ roomId }) => {
  stopTimer();
  stopTimeout();
  const timer = document.getElementById('on-game-timer');
  timer.innerText = '';
  addClass(timer, 'display-none');

  document.removeEventListener('keydown', onKeyDown);

  socket.emit('show_rating', { roomId });
  socket.emit('reset_all_user', roomId);
}

const onGameStart = async ({ roomId, secondsBeforeStart, textId, secondsForGame }) => {
  socket.emit('disable_room', roomId);
  socket.emit('bot_enounce_users');

  const { text } = await getTextById(textId);

  const backBtn = document.getElementById(`back-btn-${roomId}`);
  const readyBtn = document.getElementById(`ready-btn-${roomId}`);
  const typingText = document.getElementById('game-text');

  addClass(backBtn, 'display-none');
  addClass(readyBtn, 'display-none');

  const timerElement = document.getElementById('on-game-timer');

  const typedSpan = createElement({
    tagName: 'span',
    className: 'typed-text display-none'
  })
  const nextSymbolSpan = createElement({
    tagName: 'span',
    className: 'underlined-text display-none'
  })
  const emptySpan = createElement({
    tagName: 'span',
    className: 'empty-text display-none'
  })
  const timerBeforeStart = createElement({
    tagName: 'span',
    className: 'timer',
    attributes: { id: 'before-game-timer' }
  })

  typingText.append(typedSpan, nextSymbolSpan, emptySpan);
  arena.append(timerBeforeStart, typingText);

  let secondsToStartLefts = secondsBeforeStart;
  const beforeTimerId = setInterval(() => {
    timerBeforeStart.innerText = secondsToStartLefts;
    secondsToStartLefts -= 1;
  }, 1000);

  emptySpan.innerText = text;
  const typedKeys = [];
  const emptyKeys = text.split('');

  const typeSymbol = (key) => {
    const nextIndex = typedKeys.length;
    if (key !== emptyKeys[nextIndex]) {
      return;
    }
    typedKeys.push(key);
    const typedText = typedKeys.join('');
    const nextSymbol = emptyKeys[nextIndex + 1] || '';
    const emptyText = emptyKeys.join('').slice(nextIndex + 2);

    typedSpan.innerText = typedText;
    nextSymbolSpan.innerText = nextSymbol;
    emptySpan.innerText = emptyText;

    const progressPercentage = getProgressPercentage(typedText.length, text.length);

    socket.emit('set_indicator', { roomId, userId: socket.id, progressPercentage });
    socket.emit('update_user', { id: socket.id, progressPercentage });
  }

  onKeyDown = (event) => {
    const { key } = event;
    typeSymbol(key);
  }

  setTimeout(() => {
    clearInterval(beforeTimerId);
    addClass(timerBeforeStart, 'display-none');

    removeClass(timerElement, 'display-none');
    removeClass(typingText, 'display-none');
    removeClass(typedSpan, 'display-none');
    removeClass(nextSymbolSpan, 'display-none');
    removeClass(emptySpan, 'display-none');

    let secondsToEnd = secondsForGame;
    gameTimerId = setInterval(() => {
      timerElement.innerText = `${secondsToEnd} seconds left`;
      secondsToEnd -= 1;
    }, 1000);

    document.addEventListener('keydown', onKeyDown);
    
    timeoutId = setTimeout(() => {
      socket.emit('game_finished', { roomId });
    }, secondsForGame * 1000 + 100);

  }, secondsBeforeStart * 1000 + 100);
}

const onUserLeftRoom = () => {
  socket.emit('refresh_rooms');
  socket.emit('user_ready');
}

socket.on('UPDATE_ROOM', onUpdateRoom);
socket.on("UPDATE_ROOMS", updateRooms);
socket.on("JOIN_ROOM_DONE", joinRoomDone);
socket.on('user_left_room', onUserLeftRoom);
socket.on('show_message', message => window.alert(message));
socket.on('render_room', roomId => fillRoomInfo(socket, roomId));

socket.on('set_indecator_done', indicatorDone)
socket.on('show_rating_done', onShowModal);
socket.on('end_the_game', onGameEnd);
socket.on('start_game', onGameStart);
