import { createElement, removeClass, addClass } from '../helper.mjs';
import { showModal } from './modal.mjs';

function renderUser(user, index) {
    const userEl = createElement({
        tagName: 'span'
    })
    userEl.innerText = `#${index + 1}. ${user.username}`;
    return userEl;
}

export function showRatingModal(users, roomId) {
    const renderedUsers = users
        .sort((user1, user2) => user2.progressPercentage - user1.progressPercentage)
        .map(renderUser);

    const title = `Game Rating`;

    const attributes = {
        title,
    };
    const bodyElement = createElement({
        tagName: 'div',
        className: 'rating-list',
        attributes
    });

    bodyElement.append(...renderedUsers);
    const onClose = () => {
        const backBtn = document.getElementById(`back-btn-${roomId}`);
        const readyBtn = document.getElementById(`ready-btn-${roomId}`);
        const gameText = document.getElementById('game-text');

        gameText.innerHTML = '';
        readyBtn.innerText = 'Ready';

        addClass(gameText, 'display-none');
        removeClass(backBtn, 'display-none');
        removeClass(readyBtn, 'display-none');
    }
    showModal({ title, bodyElement, onClose });
}