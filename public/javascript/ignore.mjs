
// const startGame = async (text, secondsForGame, userId, roomId) => {
//   socket.emit('set_user_data', { id: userId, progressPercentage: 0 });
//   socket.emit('game_started', { roomId });
//   arena.innerHTML = '';
//   const timerElement = createElement({
//     tagName: 'div',
//     className: 'on-game-timer',
//     attributes: { id: 'on-game-timer' }
//   });

//   const typingText = createElement({
//     tagName: 'p',
//     className: 'inline'
//   });
//   const typedSpan = createElement({
//     tagName: 'span',
//     className: 'typed-text'
//   })
//   const nextSymbolSpan = createElement({
//     tagName: 'span',
//     className: 'underlined-text'
//   })
//   const emptySpan = createElement({
//     tagName: 'span',
//     className: 'empty-text'
//   })

//   emptySpan.innerText = text;
//   typingText.append(typedSpan, nextSymbolSpan, emptySpan);
//   const typedKeys = [];
//   const emptyKeys = text.split('');

//   const getProgressPercentage = (length, defaultLength) => {
//     return 100 * length / defaultLength;
//   }

//   const typeSymbol = (key) => {
//     const nextIndex = typedKeys.length;
//     if (key !== emptyKeys[nextIndex]) {
//       return;
//     }
//     typedKeys.push(key);
//     const typedText = typedKeys.join('');
//     const nextSymbol = emptyKeys[nextIndex + 1] || '';
//     const emptyText = emptyKeys.join('').slice(nextIndex + 2);

//     typedSpan.innerText = typedText;
//     nextSymbolSpan.innerText = nextSymbol;
//     emptySpan.innerText = emptyText;

//     const progressPercentage = getProgressPercentage(typedText.length, text.length);

//     socket.emit('set_indicator', { roomId, userId, progressPercentage });
//     socket.emit('update_user', { id: userId, progressPercentage });
//   }
//   onKeyDown = (event) => {
//     const { key } = event;
//     typeSymbol(key);
//   }
//   document.addEventListener('keydown', onKeyDown)

//   arena.append(timerElement, typingText);

//   let secondsToEnd = secondsForGame;
//   gameTimerId = setInterval(() => {
//     timerElement.innerText = `${secondsToEnd} seconds left`;
//     secondsToEnd -= 1;
//   }, 1000);

// }

// const startTimer = async ({ roomId, secondsBeforeStart, textId, secondsForGame }) => {
//   socket.emit('disable_room', roomId);
//   const backBtn = document.getElementById(`back-btn-${roomId}`);
//   const readyBtn = document.getElementById(`ready-btn-${roomId}`);

//   addClass(backBtn, 'display-none');
//   addClass(readyBtn, 'display-none');

//   const timerElement = createElement({
//     tagName: 'span',
//     className: 'timer',
//     attributes: { id: 'before-game-timer' }
//   })

//   let secondsLefts = secondsBeforeStart;
//   arena.innerHTML = '';
//   arena.append(timerElement);

//   const { text } = await getTextById(textId);

//   const gameTimerId = setInterval(() => {
//     timerElement.innerText = secondsLefts;
//     secondsLefts -= 1;
//   }, 1000);
//   setTimeout(() => {
//     clearInterval(gameTimerId);
//     arena.innerHTML = '';
//     startGame(text, secondsForGame, socket.id, roomId);
//   }, secondsBeforeStart * 1000 + 100);
// }