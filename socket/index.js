import { MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME } from "./config";
import { isAllUsersReady, getRandomIndex, getRooms, isUserActive, getUserCount, getUsersFromRoom, updateUser } from './helper';
import { texts } from '../data';

let timeoutId = null;
const roomsMap = new Map();

const updateRooms = (io, rooms) => {
  rooms.forEach((room, roomId) => {
    const userCount = getUserCount(io, roomId);
    if (userCount === 0) {
      roomsMap.delete(roomId);
      return;
    }
    if (userCount >= MAXIMUM_USERS_FOR_ONE_ROOM || roomsMap.get(roomId).timerOn) {
      const room = roomsMap.get(roomId);
      roomsMap.set(roomId, { ...room, disable: true });
    } else {
      const room = roomsMap.get(roomId);
      roomsMap.set(roomId, { ...room, disable: false });
    }
    if (userCount !== room.userCount) {
      roomsMap.set(roomId, { roomId, userCount });
    }
    if (roomsMap.has(roomId)) {
      const connectedUsers = getUsersFromRoom(io, roomId);
      io.in(roomId).emit('UPDATE_ROOM', { roomId, connectedUsers, secondsBeforeStart: SECONDS_TIMER_BEFORE_START_GAME });
    }
  })
  io.emit('UPDATE_ROOMS', getRooms(roomsMap));
}

export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;

    if (isUserActive(io, username, socket.id)) {
      socket.emit('USER_ACTIVE', username);
    }

    socket.emit('UPDATE_ROOMS', getRooms(roomsMap));

    socket.on('CREATE_ROOM', roomId => {
      if (roomsMap.has(roomId)) {
        socket.emit('show_message', 'Room name is not unique!\nTry one more time)');
        return;
      }

      socket.emit('render_room', roomId);
      socket.join(roomId, () => {
        const connectedUsers = getUsersFromRoom(io, roomId);
        socket.emit('JOIN_ROOM_DONE', { roomId, connectedUsers, username });
      });

      const userCount = getUserCount(io, roomId);
      roomsMap.set(roomId, { roomId, userCount })

      io.emit('UPDATE_ROOMS', getRooms(roomsMap));
    })

    socket.on('JOIN_ROOM', roomId => {
      const username = socket.handshake.query.username;

      socket.join(roomId, () => {
        socket.emit('render_room', roomId);
        const connectedUsers = getUsersFromRoom(io, roomId);
        io.in(roomId).emit('JOIN_ROOM_DONE', { roomId, connectedUsers, username });
        updateRooms(io, roomsMap);
      });
    })
    
    socket.on('user_ready', (roomId) => {
      socket.handshake.query.ready = !socket.handshake.query.ready;
      if (!roomId) return;
      const connectedUsers = getUsersFromRoom(io, roomId);
      io.in(roomId).emit('UPDATE_ROOM', { roomId, connectedUsers });

      if (connectedUsers.length > 1 && isAllUsersReady(connectedUsers)) {
        const textIndex = getRandomIndex(texts);

        io.in(roomId).emit('start_game', { roomId, secondsBeforeStart: SECONDS_TIMER_BEFORE_START_GAME, textId: textIndex, secondsForGame: SECONDS_FOR_GAME });

        const room = roomsMap.get(roomId);
        roomsMap.set(roomId, { ...room, disable: true, timerOn: true });
        updateRooms(io, roomsMap);
      }
    })
    socket.on("disconnect", () => {
      socket.emit('user_left_room');
      updateRooms(io, roomsMap);
    });

    socket.on('refresh_rooms', () => {
      updateRooms(io, roomsMap);
    })

    socket.on('disable_room', (roomId) => {
      if (!roomsMap.has(roomId)) return;
      const room = roomsMap.get(roomId);
      room.disable = true;
      roomsMap.set(roomId, room);
      updateRooms(io, roomsMap);
    })

    socket.on('set_indicator', ({ userId, progressPercentage, roomId }) => {
      io.in(roomId).emit('set_indecator_done', { userId, progressPercentage, roomId });
    })

    socket.on('show_rating', ({ roomId }) => {
      const connectedUsers = getUsersFromRoom(io, roomId);
      socket.emit('show_rating_done', { roomId, connectedUsers });
    })

    socket.on('game_finished', ({ roomId }) => {
      const connectedUsers = getUsersFromRoom(io, roomId);
      socket.emit('end_the_game', { roomId, connectedUsers });
    })

    socket.on('user_finished', ({ userId, roomId }) => {
      updateUser(io, { id: userId, finished: true });
      const connectedUsers = getUsersFromRoom(io, roomId);
      const allFinished = connectedUsers.every(user => user.finished);
      if (allFinished) {
        // if (timeoutId) clearTimeout(timeoutId);
        socket.emit('end_the_game', { roomId, connectedUsers });
        return;
      }
    })

    socket.on('reset_all_user', (roomId) => {
      const connectedUsers = getUsersFromRoom(io, roomId);
      connectedUsers.forEach(({ id }) => updateUser(io, { id, ready: false, finished: false, progressPercentage: 0 }));

      io.in(roomId).emit('UPDATE_ROOM', { roomId, connectedUsers });
    })

    socket.on('update_user', (userData) => {
      updateUser(io, userData);
    })
  });
};
