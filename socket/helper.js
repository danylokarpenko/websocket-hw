import User from '../services/user';

export const isAllUsersReady = (users) => users.every(user => user.ready);

export const getRandomIndex = (array) => {
    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    }
    return getRandomInt(0, array.length);
}

export const getRooms = (rooms) => {
    const res = [];
    rooms.forEach(value => res.push(value));
    return res;
};

export const isUserActive = (io, username, id) => Object.keys(io.sockets.connected)
    .find(socketId => io.sockets.connected[socketId].handshake.query.username === username && id !== socketId);

export const getUserCount = (io, roomId) => {
    const clients = io.sockets.adapter.rooms[roomId];
    if (!clients) return 0;
    return clients.length;
}

export const getUsersFromRoom = (io, roomId) => {
    const sockets = io.sockets.adapter.rooms[roomId].sockets;

    const connectedUsers = Object.keys(sockets).map(socketId => {
        const { username, ready, progressPercentage, finished } = io.sockets.connected[socketId].handshake.query;

        return new User(socketId, username, ready, progressPercentage, finished);
    })

    return connectedUsers;
}

export const updateUser = (io, user) => {
    const { id } = user;
    const userData = io.sockets.connected[id].handshake.query;
    Object.assign(userData, user);
}